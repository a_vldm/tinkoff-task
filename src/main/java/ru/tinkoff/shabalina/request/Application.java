package ru.tinkoff.shabalina.request;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Application {
    private static final String JSON_ID_PROPERTY = "APPLICATION_ID";
    private static final String JSON_CONTACT_ID_PROPERTY = "CONTACT_ID";
    private static final String JSON_DATE_PROPERTY = "DT_CREATED";
    private static final String JSON_PRODUCT_PROPERTY = "PRODUCT_NAME";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private long contactId;
    private Date dateCreated;
    private String productName;

    // For spring
    public Application() {
        id = -1;
        dateCreated = new Date();
        productName = "";
    }

    @JsonCreator
    public Application(@JsonProperty(JSON_ID_PROPERTY) long id,
                       @JsonProperty(JSON_CONTACT_ID_PROPERTY) long contactId,
                       @JsonProperty(JSON_DATE_PROPERTY) Date dateCreated,
                       @JsonProperty(JSON_PRODUCT_PROPERTY) String productName) {
        this.id = id;
        this.contactId = contactId;
        this.dateCreated = dateCreated;
        this.productName = productName;
    }

    @JsonProperty(JSON_ID_PROPERTY)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @JsonProperty(JSON_CONTACT_ID_PROPERTY)
    public long getContactId() {
        return contactId;
    }

    public void setContactId(long contactId) {
        this.contactId = contactId;
    }

    @JsonProperty(JSON_DATE_PROPERTY)
    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    @JsonProperty(JSON_PRODUCT_PROPERTY)
    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }
}
