package ru.tinkoff.shabalina.request;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ApplicationRepository extends JpaRepository<Application, Long> {
    Application findTopByContactIdOrderByDateCreatedDesc(long id);
}
