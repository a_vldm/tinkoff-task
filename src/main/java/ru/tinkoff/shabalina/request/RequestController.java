package ru.tinkoff.shabalina.request;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RequestController {
    private static final Logger LOG = LoggerFactory.getLogger(RequestController.class);
    private final ApplicationRepository repository;

    @Autowired
    public RequestController(ApplicationRepository repository) {
        this.repository = repository;
    }

    @RequestMapping("request")
    public ResponseEntity getLastRequest(@RequestParam("CONTACT_ID") String contactId) {
        try {
            Application result = repository.findTopByContactIdOrderByDateCreatedDesc(Long.valueOf(contactId));
            if (result == null) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
            } else {
                return ResponseEntity.ok(result);
            }
        } catch (DataAccessException e) {
            LOG.error("could not get application from database", e);
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).build();
        }
    }
}
