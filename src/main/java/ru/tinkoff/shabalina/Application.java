package ru.tinkoff.shabalina;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import ru.tinkoff.shabalina.request.ApplicationRepository;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@SpringBootApplication
public class Application implements CommandLineRunner {
    private static final Logger LOG = LoggerFactory.getLogger(Application.class);
    private static final boolean FILL_WITH_TEST_DATA = true;
    private static final SimpleDateFormat DATE_FORMATTER = new SimpleDateFormat("dd/MM/yyyy");

    private final ApplicationRepository repository;

    @Autowired
    public Application(ApplicationRepository repository) {
        this.repository = repository;
    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        if (!FILL_WITH_TEST_DATA) return;

        repository.save(new ru.tinkoff.shabalina.request.Application(1, 0, parse("01/01/2017"), "product 1"));
        repository.save(new ru.tinkoff.shabalina.request.Application(2, 0, parse("01/02/2017"), "product 2"));
        repository.save(new ru.tinkoff.shabalina.request.Application(3, 1, parse("02/01/2017"), "product 3"));
        repository.save(new ru.tinkoff.shabalina.request.Application(4, 2, parse("12/01/2017"), "product 4"));
        repository.save(new ru.tinkoff.shabalina.request.Application(5, 2, parse("01/01/2018"), "product 5"));
    }

    private static Date parse(String date) {
        try {
            return DATE_FORMATTER.parse(date);
        } catch (ParseException e) {
            LOG.error("could not parse date:" + date, e);
            return null;
        }
    }
}
