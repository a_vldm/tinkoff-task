import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.Test;
import ru.tinkoff.shabalina.request.Application;

import java.util.Date;

public class ApplicationUnitTest {
    @Test
    public void serializeDeserializeTest() throws JsonProcessingException {
        Application app = new Application(1, 1, new Date(), "Hello magic!");
        ObjectMapper mapper = new ObjectMapper();
        Assert.assertTrue(mapper.canSerialize(Application.class));
        String json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(app);
        Assert.assertTrue(json.contains("\"PRODUCT_NAME\" : \"Hello magic!\""));
    }
}
